package org.elsys.kristyanYochev.cflat;

import org.elsys.kristyanYochev.cflat.vm.Interpreter;

public class ErrorHandler {
    private boolean hadError = false;
    private boolean hadRuntimeError = false;

    public void error(Token where, String message) {
        if (where == null) {
            report(0, "", "", message);
            return;
        }
        if (where.type == TokenType.EOF) {
            report(where.line, where.filename, "at end", message);
        } else {
            report(where.line, where.filename, "at '" + where.lexeme + '\'', message);
        }
    }

    public boolean hadError() {
        return hadError;
    }

    public boolean hadRuntimeError() {
        return hadRuntimeError;
    }

    public void error(int line, String filename, String message) {
        report(line, filename, "", message);
    }

    public void runtimeError(Interpreter.RuntimeError error) {
        System.err.println("[line " + error.token.line + " in '" + error.token.filename + "'] " + error.getMessage());
        hadRuntimeError = true;
    }

    private void report(int line, String filename, String where, String message) {
        System.err.println("[line " + line + " in '" + filename + "'] Error " + where + ": " + message);
        hadError = true;
    }
}
