package org.elsys.kristyanYochev.cflat;

public class Token {
    public final TokenType type;
    public final String lexeme;
    public final String filename;
    public final int line;

    public Token(TokenType type, String lexeme, String filename, int line) {
        this.type = type;
        this.lexeme = lexeme;
        this.filename = filename;
        this.line = line;
    }

    @Override
    public String toString() {
        return "<" + type + " '" + lexeme + "'>";
    }
}
