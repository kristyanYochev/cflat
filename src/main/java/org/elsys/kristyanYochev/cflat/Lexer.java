package org.elsys.kristyanYochev.cflat;

import java.util.*;

import static org.elsys.kristyanYochev.cflat.TokenType.*;

public class Lexer {
    private static final Map<String, TokenType> reservedWords = new HashMap<>();

    static {
        reservedWords.put("and", AND);
        reservedWords.put("or", OR);
        reservedWords.put("true", TRUE);
        reservedWords.put("false", FALSE);
        reservedWords.put("let", LET);
        reservedWords.put("func", FUNC);
        reservedWords.put("mut", MUT);
        reservedWords.put("return", RETURN);
        reservedWords.put("if", IF);
        reservedWords.put("else", ELSE);
        reservedWords.put("while", WHILE);
        reservedWords.put("for", FOR);
        reservedWords.put("class", CLASS);
        reservedWords.put("this", THIS);
        reservedWords.put("super", SUPER);
        reservedWords.put("public", PUBLIC);
        reservedWords.put("private", PRIVATE);
        reservedWords.put("null", NULL);
        reservedWords.put("actor", ACTOR);
        reservedWords.put("import", IMPORT);
        reservedWords.put("partial", PARTIAL);
        reservedWords.put("_", IGNORE);
    }

    private final String source;
    private final String filename;
    private final ErrorHandler handler;
    private final List<Token> tokens = new ArrayList<>();

    private int start = 0;
    private int current = 0;
    private int line = 1;

    public Lexer(String source, String filename, ErrorHandler handler) {
        this.source = source;
        this.filename = filename;
        this.handler = handler;
    }

    public List<Token> scanTokens() {
        while (!atEnd()) {
            start = current;
            scanToken();
        }

        tokens.add(new Token(EOF, "", filename, line));
        return tokens;
    }

    private void scanToken() {
        char c = advance();

        switch (c) {
            case '(': addToken(LEFT_PAREN); break;
            case ')': addToken(RIGHT_PAREN); break;
            case '+': addToken(PLUS); break;
            case '-': addToken(MINUS); break;
            case '*': addToken(STAR); break;
            case '/':
                if (match('/')) {
                    while (peek() != '\n' && !atEnd()) advance();
                    line++;
                } else if (match('*')) {
                    blockComment();
                } else {
                    addToken(SLASH);
                }
                break;
            case ':': addToken(COLON); break;
            case ';': addToken(SEMICOLON); break;
            case '.': addToken(DOT); break;
            case '{': addToken(LEFT_BRACE); break;
            case '}': addToken(RIGHT_BRACE); break;
            case ',': addToken(COMMA); break;
            case '%': addToken(MODULO); break;
            case '"': string(); break;
            case '=': addToken(match('=') ? EQUAL_EQUAL : EQUAL); break;
            case '!': addToken(match('=') ? BANG_EQUAL : BANG); break;
            case '>': addToken(match('=') ? GREATER_EQUAL : GREATER); break;
            case '<':
                if (match('=')) addToken(LESS_EQUAL);
                else if (match('-')) addToken(SIGNAL);
                else addToken(LESS);

                break;
            case '\n':
                line++;
            case ' ':
            case '\t':
                break;
            default:
                if (Character.isDigit(c)) {
                    number();
                } else if (Character.isAlphabetic(c) || c == '_') {
                    identifier();
                } else {
                    handler.error(line, filename, "Unexpected character '" + c + '\'');
                }
        }
    }

    private void string() {
        while (peek() != '"' && !atEnd()) {
            if (peek() == '\n') line++;
            advance();
        }

        if (atEnd()) {
            handler.error(line, filename, "Unterminated string.");
            return;
        }

        advance();

        addToken(STRING);
    }

    private void blockComment() {
        while (!(peek() == '*' && peekNext() == '/') && !atEnd()) {
            if (peek() == '\n') line++;
            advance();
        }

        if (atEnd()) {
            handler.error(line, filename, "Unterminated block comment");
            return;
        }

        advance();
        advance();
    }

    private void identifier() {
        while (Character.isAlphabetic(peek()) || Character.isDigit(peek()) || peek() == '_') {
            advance();
        }

        TokenType type = reservedWords.get(currentLexeme());
        addToken(type != null ? type : IDENTIFIER);
    }

    private void number() {
        while (Character.isDigit(peek())) advance();

        if (peek() == '.' && Character.isDigit(peekNext())) {
            advance();
            while (Character.isDigit(peek())) advance();
        }

        addToken(NUMBER);
    }

    private char peekNext() {
        if (current + 1 >= source.length()) return '\0';
        return source.charAt(current + 1);
    }

    private char peek() {
        if (atEnd()) return '\0';
        return source.charAt(current);
    }

    private boolean match(char c) {
        if (peek() == c) {
            advance();
            return true;
        }
        return false;
    }

    private String currentLexeme() {
        return source.substring(start, current);
    }

    private void addToken(TokenType tokenType) {
        String text = currentLexeme();
        tokens.add(new Token(tokenType, text, filename, line));
    }

    private char advance() {
        current++;
        return source.charAt(current - 1);
    }

    private boolean atEnd() {
        return current >= source.length();
    }
}
