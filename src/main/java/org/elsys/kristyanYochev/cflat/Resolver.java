package org.elsys.kristyanYochev.cflat;

import org.elsys.kristyanYochev.cflat.ast.Expr;
import org.elsys.kristyanYochev.cflat.ast.Stmt;
import org.elsys.kristyanYochev.cflat.vm.Interpreter;

import java.util.*;

public class Resolver implements Expr.Visitor<Void>, Stmt.Visitor<Void> {
    private enum FunctionType {
        NONE, FUNCTION, METHOD, INITIALIZER, MESSAGE
    }

    private enum ClassType {
        NONE,
        CLASS,
        SUBCLASS,
        ACTOR
    }

    private static class FunctionDescription {
        public final FunctionType type;
        public final boolean mut;
        public final int depth;

        public FunctionDescription(FunctionType type, boolean mut, int depth) {
            this.type = type;
            this.mut = mut;
            this.depth = depth;
        }
    }

    private static class VariableDescription {
        public final boolean mut;
        public boolean defined;

        public VariableDescription(boolean mut) {
            this.mut = mut;
            this.defined = false;
        }
    }

    private final Interpreter interpreter;
    private final ErrorHandler handler;
    private final Stack<Map<String, VariableDescription>> scopes = new Stack<>();

    private FunctionDescription currentFunctionDescription = new FunctionDescription(FunctionType.NONE, false, -1);
    private ClassType currentClassType = ClassType.NONE;

    public Resolver(Interpreter interpreter, ErrorHandler handler) {
        this.interpreter = interpreter;
        this.handler = handler;
    }

    public void resolve(List<Stmt> statements) {
        statements.forEach(this::resolve);
    }

    public void resolve(Stmt statement) {
        statement.accept(this);
    }

    public void resolve(Expr expression) {
        expression.accept(this);
    }

    private void resolveLocal(Expr expr, Token name) {
        for (int i = scopes.size() - 1; i >= 0; i--) {
            if (scopes.get(i).containsKey(name.lexeme)) {
                interpreter.resolve(expr, scopes.size() - 1 - i);
            }
        }
    }

    private void resolveAssignment(Expr expr, Token name) {
        for (int depth = scopes.size() - 1; depth >= 0; depth--) {
            if (scopes.get(depth).containsKey(name.lexeme)) {
                if (currentFunctionDescription.type != FunctionType.NONE) {
                    VariableDescription assignedVariable = scopes.get(depth).get(name.lexeme);

                    if (!assignedVariable.mut) {
                        handler.error(name, "Assignment of a immutable object!");
                    }

                    if (!currentFunctionDescription.mut && depth < currentFunctionDescription.depth) {
                        handler.error(name, "Non-mut function mutating non-local state!");
                    }
                }
                interpreter.resolve(expr, scopes.size() - 1 - depth);
            }
        }
    }

    @Override
    public Void visitBinaryExpr(Expr.Binary binary) {
        resolve(binary.left);
        resolve(binary.right);
        return null;
    }

    @Override
    public Void visitLiteralExpr(Expr.Literal literal) {
        return null;
    }

    @Override
    public Void visitGroupingExpr(Expr.Grouping grouping) {
        resolve(grouping.expression);
        return null;
    }

    @Override
    public Void visitUnaryExpr(Expr.Unary unary) {
        resolve(unary.right);
        return null;
    }

    @Override
    public Void visitLogicalExpr(Expr.Logical logical) {
        resolve(logical.left);
        resolve(logical.right);
        return null;
    }

    @Override
    public Void visitReferenceExpr(Expr.Reference reference) {
        if (!scopes.isEmpty() &&
                scopes.peek().get(reference.name.lexeme) != null &&
                !scopes.peek().get(reference.name.lexeme).defined
        ) {
            handler.error(reference.name, "Cannot read a local variable in its own initializer.");
        }

        resolveLocal(reference, reference.name);
        return null;
    }

    @Override
    public Void visitCallExpr(Expr.Call call) {
        resolve(call.callee);
        call.arguments.forEach(this::resolve);
        return null;
    }

    @Override
    public Void visitAssignmentExpr(Expr.Assignment assignment) {
        resolve(assignment.value);
        resolveAssignment(assignment, assignment.name);
        return null;
    }

    @Override
    public Void visitGetExpr(Expr.Get get) {
        resolve(get.object);
        return null;
    }

    @Override
    public Void visitSetExpr(Expr.Set set) {
        resolve(set.newValue);
        resolve(set.object);
        return null;
    }

    @Override
    public Void visitThisExpr(Expr.This thisExpr) {
        if (currentClassType == ClassType.NONE) {
            handler.error(thisExpr.keyword, "Cannot use 'this' outside of a class.");
            return null;
        }
        resolveLocal(thisExpr, thisExpr.keyword);
        return null;
    }

    @Override
    public Void visitSuperExpr(Expr.Super superExpr) {
        if (currentClassType == ClassType.NONE) {
            handler.error(superExpr.keyword, "Cannot use 'super' outside of a class.");
        } else if (currentClassType == ClassType.CLASS) {
            handler.error(superExpr.keyword, "Cannot use 'super' in a class with no superclass.");
        }

        resolveLocal(superExpr, superExpr.keyword);
        return null;
    }

    @Override
    public Void visitMemberCallExpr(Expr.MemberCall memberCall) {
        resolve(memberCall.object);
        memberCall.arguments.forEach(this::resolve);
        return null;
    }

    @Override
    public Void visitLambdaExpr(Expr.Lambda lambda) {
        FunctionDescription enclosingFunction = currentFunctionDescription;
        currentFunctionDescription = new FunctionDescription(FunctionType.FUNCTION, lambda.mut, scopes.size());

        beginScope();
        lambda.arguments.forEach(argument -> {
            declare(argument, false);
            define(argument);
        });
        resolve(lambda.body);
        endScope();

        currentFunctionDescription = enclosingFunction;
        return null;
    }

    @Override
    public Void visitPartialExpr(Expr.Partial partial) {
        resolve(partial.callee);
        partial.arguments.forEach(arg -> {
            if (arg != null) resolve(arg);
        });
        return null;
    }

    @Override
    public Void visitExpressionStatement(Stmt.Expression expression) {
        resolve(expression.expression);
        return null;
    }

    @Override
    public Void visitVarDeclaration(Stmt.VarDeclaration varDeclaration) {
        declare(varDeclaration.name, varDeclaration.mut);
        if (varDeclaration.initialValue != null) {
            resolve(varDeclaration.initialValue);
        }
        define(varDeclaration.name);
        return null;
    }

    @Override
    public Void visitFunctionDeclaration(Stmt.FunctionDeclaration functionDeclaration) {
        declare(functionDeclaration.name, false);
        define(functionDeclaration.name);

        resolveFunction(functionDeclaration, FunctionType.FUNCTION);
        return null;
    }

    @Override
    public Void visitReturnStatement(Stmt.ReturnStatement returnStatement) {
        if (currentFunctionDescription.type == FunctionType.NONE) {
            handler.error(returnStatement.keyword, "Cannot return from top-level code");
        }

        if (returnStatement.returnValue != null) {
            if (currentFunctionDescription.type == FunctionType.INITIALIZER) {
                handler.error(returnStatement.keyword, "Cannot return value from a constructor.");
            }
            resolve(returnStatement.returnValue);
        }
        return null;
    }

    @Override
    public Void visitBlock(Stmt.Block block) {
        beginScope();
        resolve(block.statements);
        endScope();
        return null;
    }

    @Override
    public Void visitIfStatement(Stmt.IfStatement ifStatement) {
        resolve(ifStatement.condition);
        resolve(ifStatement.thenBlock);
        if (ifStatement.elseBlock != null) resolve(ifStatement.elseBlock);
        return null;
    }

    @Override
    public Void visitWhile(Stmt.While whileStatement) {
        resolve(whileStatement.condition);
        resolve(whileStatement.body);
        return null;
    }

    @Override
    public Void visitClassDecl(Stmt.ClassDecl classDecl) {
        ClassType enclosingClass = currentClassType;
        currentClassType = ClassType.CLASS;

        declare(classDecl.name, false);
        define(classDecl.name);

        if (classDecl.superclass != null) {
            resolve(classDecl.superclass);
            currentClassType = ClassType.SUBCLASS;

            if (classDecl.name.lexeme.equals(classDecl.superclass.name.lexeme)) {
                handler.error(classDecl.superclass.name, "A class cannot inherit from itself.");
            }

            beginScope();
            VariableDescription superDescription = new VariableDescription(false);
            superDescription.defined = true;
            scopes.peek().put("super", superDescription);
        }

        beginScope();
        VariableDescription thisDescription = new VariableDescription(false);
        thisDescription.defined = true;
        scopes.peek().put("this", thisDescription);

        classDecl.methods.forEach(methodMember -> {
            Stmt.FunctionDeclaration method = methodMember.member;

            FunctionType methodDeclType = FunctionType.METHOD;
            if (method.name.lexeme.equals("constructor")) {
                methodDeclType = FunctionType.INITIALIZER;
            }
            resolveFunction(method, methodDeclType);
        });

        endScope();

        if (classDecl.superclass != null) endScope();

        currentClassType = enclosingClass;
        return null;
    }

    @Override
    public Void visitActorDecl(Stmt.ActorDecl actorDecl) {
        ClassType enclosingClass = currentClassType;
        currentClassType = ClassType.CLASS;

        declare(actorDecl.name, false);
        define(actorDecl.name);

        beginScope();
        VariableDescription thisDescription = new VariableDescription(false);
        thisDescription.defined = true;
        scopes.peek().put("this", thisDescription);

        actorDecl.methods.forEach(method -> resolveFunction(
                method,
                method.name.lexeme.equals("constructor") ? FunctionType.INITIALIZER : FunctionType.METHOD
            )
        );

        actorDecl.messages.forEach(message -> resolveFunction(message, FunctionType.MESSAGE));

        endScope();
        currentClassType = enclosingClass;

        return null;
    }

    @Override
    public Void visitSignal(Stmt.Signal signal) {
        resolve(signal.actorInstance);
        signal.arguments.forEach(this::resolve);
        return null;
    }

    @Override
    public Void visitImport(Stmt.Import importStmt) {
        importStmt.names.forEach(name -> {
            declare(name, false);
            define(name);
        });
        return null;
    }

    private void resolveFunction(Stmt.FunctionDeclaration functionDeclaration, FunctionType type) {
        FunctionDescription enclosingFunction = currentFunctionDescription;
        currentFunctionDescription = new FunctionDescription(type, functionDeclaration.mut, scopes.size());

        beginScope();
        functionDeclaration.arguments.forEach(argument -> {
            declare(argument, false);
            define(argument);
        });
        resolve(functionDeclaration.body);
        endScope();

        currentFunctionDescription = enclosingFunction;
    }

    private void beginScope() {
        scopes.push(new HashMap<>());
    }

    private void endScope() {
        scopes.pop();
    }

    private void declare(Token name, boolean mutable) {
        if (scopes.isEmpty()) return;
        if (scopes.peek().containsKey(name.lexeme)) return;

        scopes.peek().put(name.lexeme, new VariableDescription(mutable));
    }

    private void define(Token name) {
        if (scopes.isEmpty()) return;

        scopes.peek().get(name.lexeme).defined = true;
    }
}
