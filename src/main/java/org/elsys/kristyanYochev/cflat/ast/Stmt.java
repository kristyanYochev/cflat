package org.elsys.kristyanYochev.cflat.ast;

import org.elsys.kristyanYochev.cflat.Token;

import java.util.List;

public abstract class Stmt {
    public interface Visitor<ReturnType> {
        ReturnType visitExpressionStatement(Expression expression);
        ReturnType visitVarDeclaration(VarDeclaration varDeclaration);
        ReturnType visitFunctionDeclaration(FunctionDeclaration functionDeclaration);
        ReturnType visitReturnStatement(ReturnStatement returnStatement);
        ReturnType visitBlock(Block block);
        ReturnType visitIfStatement(IfStatement ifStatement);
        ReturnType visitWhile(While whileStatement);
        ReturnType visitClassDecl(ClassDecl classDecl);
        ReturnType visitActorDecl(ActorDecl actorDecl);
        ReturnType visitSignal(Signal signal);
        ReturnType visitImport(Import importStmt);
    }

    public abstract <ReturnType> ReturnType accept(Visitor<ReturnType> visitor);

    public static class Expression extends Stmt {
        public final Expr expression;

        public Expression(Expr expression) {
            this.expression = expression;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitExpressionStatement(this);
        }
    }

    public static class VarDeclaration extends Stmt {
        public final Token name;
        public final Expr initialValue;
        public final boolean mut;

        public VarDeclaration(Token name, Expr initialValue, boolean mut) {
            this.name = name;
            this.initialValue = initialValue;
            this.mut = mut;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitVarDeclaration(this);
        }
    }

    public static class FunctionDeclaration extends Stmt {
        public final Token name;
        public final List<Token> arguments;
        public final List<Stmt> body;
        public final boolean mut;

        public FunctionDeclaration(Token name, List<Token> arguments, List<Stmt> body, boolean mut) {
            this.name = name;
            this.arguments = arguments;
            this.body = body;
            this.mut = mut;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitFunctionDeclaration(this);
        }
    }

    public static class ReturnStatement extends Stmt {
        public final Token keyword;
        public final Expr returnValue;

        public ReturnStatement(Token keyword, Expr returnValue) {
            this.keyword = keyword;
            this.returnValue = returnValue;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitReturnStatement(this);
        }
    }

    public static class Block extends Stmt {
        public final List<Stmt> statements;

        public Block(List<Stmt> statements) {
            this.statements = statements;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitBlock(this);
        }
    }

    public static class IfStatement extends Stmt {
        public final Token keyword;
        public final Expr condition;
        public final Stmt thenBlock;
        public final Stmt elseBlock;

        public IfStatement(Token keyword, Expr condition, Stmt thenBlock, Stmt elseBlock) {
            this.keyword = keyword;
            this.condition = condition;
            this.thenBlock = thenBlock;
            this.elseBlock = elseBlock;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitIfStatement(this);
        }
    }

    public static class While extends Stmt {
        public final Expr condition;
        public final Stmt body;

        public While(Expr condition, Stmt body) {
            this.condition = condition;
            this.body = body;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitWhile(this);
        }
    }

    public static class ClassDecl extends Stmt {
        public static class Member<T> {
            public final boolean isPublic;
            public final T member;

            public Member(boolean isPublic, T member) {
                this.isPublic = isPublic;
                this.member = member;
            }
        }

        public final Token name;
        public final Expr.Reference superclass;
        public final List<Member<FunctionDeclaration>> methods;
        public final List<Member<VarDeclaration>> fields;

        public ClassDecl(Token name, Expr.Reference superclass, List<Member<FunctionDeclaration>> methods, List<Member<VarDeclaration>> fields) {
            this.name = name;
            this.superclass = superclass;
            this.methods = methods;
            this.fields = fields;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitClassDecl(this);
        }
    }

    public static class ActorDecl extends Stmt {
        public final Token name;
        public final List<VarDeclaration> fields;
        public final List<FunctionDeclaration> methods;
        public final List<FunctionDeclaration> messages;

        public ActorDecl(Token name, List<VarDeclaration> fields, List<FunctionDeclaration> methods, List<FunctionDeclaration> messages) {
            this.name = name;
            this.fields = fields;
            this.methods = methods;
            this.messages = messages;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitActorDecl(this);
        }
    }

    public static class Signal extends Stmt {
        public final Expr actorInstance;
        public final Token messageName;
        public final List<Expr> arguments;

        public Signal(Expr actorInstance, Token messageName, List<Expr> arguments) {
            this.actorInstance = actorInstance;
            this.messageName = messageName;
            this.arguments = arguments;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitSignal(this);
        }
    }

    public static class Import extends Stmt {
        public final Token modulePath;
        public final List<Token> names;

        public Import(Token modulePath, List<Token> names) {
            this.modulePath = modulePath;
            this.names = names;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitImport(this);
        }
    }
}
