package org.elsys.kristyanYochev.cflat.ast;

import org.elsys.kristyanYochev.cflat.Token;

import java.util.List;

public abstract class Expr {
    public interface Visitor<ReturnType> {
        ReturnType visitBinaryExpr(Binary binary);
        ReturnType visitLiteralExpr(Literal literal);
        ReturnType visitGroupingExpr(Grouping grouping);
        ReturnType visitUnaryExpr(Unary unary);
        ReturnType visitLogicalExpr(Logical logical);
        ReturnType visitReferenceExpr(Reference reference);
        ReturnType visitCallExpr(Call call);
        ReturnType visitAssignmentExpr(Assignment assignment);
        ReturnType visitGetExpr(Get get);
        ReturnType visitSetExpr(Set set);
        ReturnType visitThisExpr(This thisExpr);
        ReturnType visitSuperExpr(Super superExpr);
        ReturnType visitMemberCallExpr(MemberCall memberCall);
        ReturnType visitLambdaExpr(Lambda lambda);
        ReturnType visitPartialExpr(Partial partial);
    }

    abstract public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor);

    public static class Binary extends Expr {
        public final Expr left;
        public final Token operator;
        public final Expr right;

        public Binary(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitBinaryExpr(this);
        }
    }

    public static class Logical extends Expr {
        public final Expr left;
        public final Token operator;
        public final Expr right;

        public Logical(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitLogicalExpr(this);
        }
    }

    public static class Literal extends Expr {
        public final Object value;

        public Literal(Object value) {
            this.value = value;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitLiteralExpr(this);
        }
    }

    public static class Grouping extends Expr {
        public final Expr expression;

        public Grouping(Expr expression) {
            this.expression = expression;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitGroupingExpr(this);
        }
    }

    public static class Unary extends Expr {
        public final Token operator;
        public final Expr right;

        public Unary(Token operator, Expr right) {
            this.operator = operator;
            this.right = right;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitUnaryExpr(this);
        }
    }

    public static class Reference extends Expr {
        public final Token name;

        public Reference(Token name) {
            this.name = name;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitReferenceExpr(this);
        }
    }

    public static class Call extends Expr {
        public final Expr callee;
        public final Token paren;
        public final List<Expr> arguments;

        public Call(Expr callee, Token paren, List<Expr> arguments) {
            this.callee = callee;
            this.paren = paren;
            this.arguments = arguments;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitCallExpr(this);
        }
    }

    public static class Assignment extends Expr {
        public final Token name;
        public final Expr value;

        public Assignment(Token name, Expr value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitAssignmentExpr(this);
        }
    }

    public static class Get extends Expr {
        public final Expr object;
        public final Token name;

        public Get(Expr object, Token name) {
            this.object = object;
            this.name = name;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitGetExpr(this);
        }
    }

    public static class Set extends Expr {
        public final Expr object;
        public final Token name;
        public final Expr newValue;

        public Set(Expr object, Token name, Expr newValue) {
            this.object = object;
            this.name = name;
            this.newValue = newValue;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitSetExpr(this);
        }
    }

    public static class This extends Expr {
        public final Token keyword;

        public This(Token keyword) {
            this.keyword = keyword;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitThisExpr(this);
        }
    }

    public static class Super extends Expr {
        public final Token keyword;
        public final Token name;

        public Super(Token keyword, Token name) {
            this.keyword = keyword;
            this.name = name;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitSuperExpr(this);
        }
    }

    public static class MemberCall extends Expr {
        public final Expr object;
        public final Token memberName;
        public final List<Expr> arguments;

        public MemberCall(Expr object, Token memberName, List<Expr> arguments) {
            this.object = object;
            this.memberName = memberName;
            this.arguments = arguments;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitMemberCallExpr(this);
        }
    }

    public static class Lambda extends Expr {
        public final List<Token> arguments;
        public final List<Stmt> body;
        public final boolean mut;

        public Lambda(List<Token> arguments, List<Stmt> body, boolean mut) {
            this.arguments = arguments;
            this.body = body;
            this.mut = mut;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitLambdaExpr(this);
        }
    }

    public static class Partial extends Expr {
        public final Token keyword;
        public final Expr callee;
        public final List<Expr> arguments;

        public Partial(Token keyword, Expr callee, List<Expr> arguments) {
            this.keyword = keyword;
            this.callee = callee;
            this.arguments = arguments;
        }

        @Override
        public <ReturnType> ReturnType accept(Visitor<ReturnType> visitor) {
            return visitor.visitPartialExpr(this);
        }
    }
}
