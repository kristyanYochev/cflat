package org.elsys.kristyanYochev.cflat;

import org.elsys.kristyanYochev.cflat.ast.*;
import org.elsys.kristyanYochev.cflat.vm.builtins.*;

import java.util.*;

import static org.elsys.kristyanYochev.cflat.TokenType.*;
import static org.elsys.kristyanYochev.cflat.ast.Stmt.ClassDecl.Member;

public class Parser {
    private static class ParserError extends RuntimeException {}

    private final ErrorHandler handler;
    private final List<Token> tokens;
    private int current = 0;

    public Parser(List<Token> tokens, ErrorHandler handler) {
        this.handler = handler;
        this.tokens = tokens;
    }

    public List<Stmt> parse() {
        return program();
    }

    private List<Stmt> program() {
        List<Stmt> statements = new ArrayList<>();
        while (match(IMPORT)) {
            Stmt statement = importStatement();
            if (statement != null) statements.add(statement);
        }

        while (!atEnd()) {
            Stmt statement = declaration(false);
            if (statement != null) statements.add(statement);
        }

        return statements;
    }

    private Stmt declaration(boolean allowStatements) {
        try {
            if (match(LET)) return varDeclaration(allowStatements);
            if (match(FUNC)) return functionDeclaration("function");
            if (match(CLASS)) return classDeclaration();
            if (match(ACTOR)) return actorDeclaration();

            if (allowStatements) return statement();
            else throw error(peek(), "Statements are not allowed here.");
        } catch (ParserError error) {
            synchronize();
            return null;
        }
    }

    private Stmt importStatement() {
        Token path = consume(STRING, "Expect module path name.");
        consume(FOR, "Expect 'for' after module path.");

        List<Token> importedNames = new ArrayList<>();

        do {
            importedNames.add(consume(IDENTIFIER, "Expect identifier as import name."));
        } while (match(COMMA));

        consume(SEMICOLON, "Expect ';' after import statement.");

        return new Stmt.Import(path, importedNames);
    }

    private Stmt actorDeclaration() {
        Token name = consume(IDENTIFIER, "Expect actor name.");

        consume(LEFT_BRACE, "Expect '{' before actor body.");

        List<Stmt.VarDeclaration> fields = new ArrayList<>();
        List<Stmt.FunctionDeclaration> methods = new ArrayList<>();
        List<Stmt.FunctionDeclaration> messages = new ArrayList<>();

        while (!check(RIGHT_BRACE) && !atEnd()) {
            if (match(LET)) fields.add(varDeclaration(true));
            else if (match(FUNC)) methods.add(functionDeclaration("method"));
            else messages.add(functionDeclaration("message"));
        }

        consume(RIGHT_BRACE, "Expect '}' after actor body.");

        return new Stmt.ActorDecl(name, fields, methods, messages);
    }

    private Stmt classDeclaration() {
        Token name = consume(IDENTIFIER, "Expect class name.");

        Expr.Reference superclass = match(COLON) ?
            new Expr.Reference(consume(IDENTIFIER, "Expect superclass name.")) :
            null;

        consume(LEFT_BRACE, "Expect '{' before class body.");

        List<Member<Stmt.FunctionDeclaration>> methods = new ArrayList<>();
        List<Member<Stmt.VarDeclaration>> fields = new ArrayList<>();

        while (!check(RIGHT_BRACE) && !atEnd()) {
            boolean isPublic = false;
            if (match(PUBLIC) || match(PRIVATE)) {
                isPublic = previous().type == PUBLIC;
            }

            if (match(FUNC)) methods.add(new Member<>(isPublic, functionDeclaration("method")));
            else fields.add(new Member<>(isPublic, varDeclaration(true)));
        }

        consume(RIGHT_BRACE, "Expect '{' after class body.");
        return new Stmt.ClassDecl(name, superclass, methods, fields);
    }

    private Stmt.FunctionDeclaration functionDeclaration(String type) {
        boolean mut = match(MUT);
        Token name = consume(IDENTIFIER, "Expected " + type + " name.");

        consume(LEFT_PAREN, "Expected '(' after " + type + " name.");
        List<Token> arguments = new ArrayList<>();

        if (!check(RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 255) {
                    //noinspection ThrowableNotThrown
                    error(peek(), "Cannot have more than 255 parameters.");
                }

                arguments.add(consume(IDENTIFIER, "Expected identifier as function argument."));
            } while (match(COMMA));
        }

        consume(RIGHT_PAREN, "Expect ')' after arguments.");
        consume(LEFT_BRACE, "Expected '{' after argument list.");
        List<Stmt> body = block();
        return new Stmt.FunctionDeclaration(name, arguments, body, mut);
    }

    private List<Stmt> block() {
        List<Stmt> statements = new ArrayList<>();
        while (!check(RIGHT_BRACE) && !atEnd()) {
            statements.add(declaration(true));
        }

        consume(RIGHT_BRACE, "Expected '}' after block");
        return statements;
    }

    private Stmt.VarDeclaration varDeclaration(boolean allowMut) {
        boolean mut = match(MUT);
        if (mut && !allowMut) {
            //noinspection ThrowableNotThrown
            error(peek(), "Mutable variables not allowed here.");
        }
        Token name = consume(IDENTIFIER, "Expected variable name.");

        Expr initializer = null;
        if (match(EQUAL)) {
            initializer = expression();
        }
        consume(SEMICOLON, "Expect ';' after variable declaration.");

        return new Stmt.VarDeclaration(name, initializer, mut);
    }

    private Stmt statement() {
        if (match(RETURN)) return returnStatement();
        if (match(IF)) return ifStatement();
        if (match(WHILE)) return whileStatement();
        if (match(FOR)) return forStatement();

        if (match(LEFT_BRACE)) return blockStatement();

        return expressionStatement();
    }

    private Stmt returnStatement() {
        Token keyword = previous();

        if (match(SEMICOLON)) {
            return new Stmt.ReturnStatement(keyword, null);
        }

        Expr expr = expression();
        consume(SEMICOLON, "Expect ';' after return value.");

        return new Stmt.ReturnStatement(keyword, expr);
    }

    private Stmt ifStatement() {
        Token keyword = previous();

        consume(LEFT_PAREN, "Expected '(' before if condition.");
        Expr condition = expression();
        consume(RIGHT_PAREN, "Expected ')' after if condition");

        Stmt thenBranch = statement();
        Stmt elseBranch = null;
        if (match(ELSE)) {
            elseBranch = statement();
        }

        return new Stmt.IfStatement(keyword, condition, thenBranch, elseBranch);
    }

    private Stmt whileStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'while'.");
        Expr condition = expression();
        consume(RIGHT_PAREN, "Expect ')' after loop condition.");
        Stmt body = statement();

        return new Stmt.While(condition, body);
    }

    private Stmt forStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'for'.");
        Stmt initializer = null;

        if (match(LET)) initializer = varDeclaration(true);
        else if (!match(SEMICOLON)) initializer = expressionStatement();

        Expr condition = check(SEMICOLON) ? null : expression();
        consume(SEMICOLON, "Expect ';' after for loop condition.");

        Expr increment = check(RIGHT_PAREN) ? null : expression();
        consume(RIGHT_PAREN, "Expect ')' after for clauses.");

        Stmt body = statement();

        if (increment != null) body = new Stmt.Block(List.of(body, new Stmt.Expression(increment)));

        if (condition == null) condition = new Expr.Literal(CFlatBoolClass.TRUE);
        body = new Stmt.While(condition, body);

        if (initializer != null) body = new Stmt.Block(List.of(initializer, body));

        return body;
    }

    private Stmt blockStatement() {
        List<Stmt> statements = block();

        return new Stmt.Block(statements);
    }

    private Stmt expressionStatement() {
        Expr expression = expression();

        if (match(SIGNAL)) {
            Token messageName = consume(IDENTIFIER, "Expect message name after '<-'.");
            consume(LEFT_PAREN, "Expect '(' after message name.");

            List<Expr> arguments = new ArrayList<>();

            if (!check(RIGHT_PAREN)) {
                do {
                    arguments.add(expression());
                } while (match(COMMA));
            }

            consume(RIGHT_PAREN, "Expect ')' after message parameters.");
            consume(SEMICOLON, "Expect ';' after signal statement.");

            return new Stmt.Signal(expression, messageName, arguments);
        }

        consume(SEMICOLON, "Expect ';' after expression statement.");
        return new Stmt.Expression(expression);
    }

    private Expr expression() {
        return assignment();
    }

    private Expr assignment() {
        Expr expr = logicalOr();

        if (match(EQUAL)) {
            Token equalSign = previous();
            Expr value = assignment();

            if (expr instanceof Expr.Reference) {
                Token name = ((Expr.Reference)expr).name;
                return new Expr.Assignment(name, value);
            } else if (expr instanceof Expr.Get) {
                Expr.Get get = (Expr.Get)expr;
                return new Expr.Set(get.object, get.name, value);
            }

            //noinspection ThrowableNotThrown
            error(equalSign, "Invalid assignment target.");
        }

        return expr;
    }

    private Expr logicalOr() {
        Expr expr = logicalAnd();

        while (match(OR)) {
            Token operator = previous();
            Expr right = logicalAnd();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr logicalAnd() {
        Expr expr = equality();

        while (match(AND)) {
            Token operator = previous();
            Expr right = equality();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr equality() {
        Expr expr = comparison();

        while (match(EQUAL_EQUAL, BANG_EQUAL)) {
            Token operator = previous();
            Expr right = comparison();

            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr comparison() {
        Expr expr = addition();

        while (match(LESS, LESS_EQUAL, GREATER, GREATER_EQUAL)) {
            Token operator = previous();
            Expr right = addition();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr addition() {
        Expr expr = multiplication();

        while (match(PLUS, MINUS)) {
            Token operator = previous();
            Expr right = multiplication();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr multiplication() {
        Expr expr = unary();

        while (match(SLASH, STAR, MODULO)) {
            Token operator = previous();
            Expr right = unary();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr unary() {
        if (match(MINUS, BANG)) {
            Token operator = previous();
            Expr right = unary();
            return new Expr.Unary(operator, right);
        }

        return call();
    }

    private Expr call() {
        Expr expr = primary();
        while (true) {
            if (match(LEFT_PAREN)) {
                expr = finishCall(expr);
            } else if (match(DOT)) {
                Token name = consume(IDENTIFIER, "Expect property name after '.'.");
                expr = new Expr.Get(expr, name);
            } else {
                break;
            }
        }

        return expr;
    }

    private Expr finishCall(Expr callee) {
        List<Expr> args = new ArrayList<>();

        if (!check(RIGHT_PAREN)) {
            do {
                if (args.size() >= 255) {
                    //noinspection ThrowableNotThrown
                    error(peek(), "Cannot have more than 255 arguments.");
                }
                args.add(expression());
            } while (match(COMMA));
        }

        Token paren = consume(RIGHT_PAREN, "Expect ')' after arguments.");

        if (callee instanceof Expr.Get) {
            return new Expr.MemberCall(
                    ((Expr.Get) callee).object,
                    ((Expr.Get) callee).name,
                    args
            );
        }

        return new Expr.Call(callee, paren, args);
    }

    private Expr primary() {
        if (match(LEFT_PAREN)) {
            Expr expr = expression();
            consume(RIGHT_PAREN, "Expected ')' after expression.");
            return new Expr.Grouping(expr);
        }

        if (match(NUMBER)) {
            String lexeme = previous().lexeme;
            return new Expr.Literal(
                lexeme.contains(".") ?
                    new CFlatFloatClass.CFlatFloat(Float.parseFloat(lexeme)) :
                    new CFlatIntClass.CFlatInteger(Integer.parseInt(lexeme))
            );
        }

        if (match(STRING)) {
            String value = previous().lexeme;
            value = value.substring(1, value.length() - 1);
            return new Expr.Literal(new CFlatStringClass.CFlatString(value));
        }

        if (match(TRUE, FALSE)) {
            return new Expr.Literal(CFlatBoolClass.javaBoolToCFlatBool(previous().type == TRUE));
        }

        if (match(IDENTIFIER)) {
            return new Expr.Reference(previous());
        }

        if (match(THIS)) {
            return new Expr.This(previous());
        }

        if (match(SUPER)) {
            Token keyword = previous();
            consume(DOT, "Expect '.' after 'super'.");
            Token name = consume(IDENTIFIER, "Expect superclass field name.");
            return new Expr.Super(keyword, name);
        }

        if (match(FUNC)) {
            return lambda();
        }

        if (match(PARTIAL)) {
            return partialApplication();
        }

        if (match(NULL)) {
            return new Expr.Literal(CFlatNullClass.theInstance);
        }

        throw error(peek(), "Expected expression.");
    }

    private Expr partialApplication() {
        Token keyword = previous();
        Expr callee = primary();

        while (match(DOT)) {
            callee = new Expr.Get(callee, consume(IDENTIFIER, "Expect property name after '.'."));
        }

        consume(LEFT_PAREN, "Expected '(' after partial expression function.");

        List<Expr> arguments = new ArrayList<>();

        if (!check(RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 255) {
                    //noinspection ThrowableNotThrown
                    error(peek(), "Cannot have more than 255 arguments.");
                }
                arguments.add(match(IGNORE) ? null : expression());
            } while (match(COMMA));
        }

        consume(RIGHT_PAREN, "Expect ')' after arguments.");

        return new Expr.Partial(keyword, callee, arguments);
    }

    private Expr lambda() {
        boolean mut = match(MUT);

        consume(LEFT_PAREN, "Expect '(' before lambda arguments.");
        List<Token> arguments = new ArrayList<>();

        while (!check(RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 255) {
                    //noinspection ThrowableNotThrown
                    error(peek(), "Cannot have more than 255 parameters.");
                }
                arguments.add(consume(IDENTIFIER, "Expected identifier as lambda argument."));
            } while (match(COMMA));
        }

        consume(RIGHT_PAREN, "Expect ')' after lambda arguments.");
        consume(LEFT_BRACE, "Expect '}' after lambda argument list.");
        List<Stmt> body = block();
        return new Expr.Lambda(arguments, body, mut);
    }

    private boolean match(TokenType... types) {
        boolean matches = Arrays.stream(types).anyMatch(this::check);
        if (matches) advance();
        return matches;
    }

    private Token consume(TokenType type, String errorMessage) {
        if (check(type)) return advance();
        throw error(peek(), errorMessage);
    }

    private boolean check(TokenType type) {
        if (atEnd()) return false;
        return peek().type == type;
    }

    private Token peek() {
        return tokens.get(current);
    }

    private boolean atEnd() {
        return peek().type == EOF;
    }

    private Token advance() {
        if (!atEnd()) current++;
        return previous();
    }

    private Token previous() {
        return tokens.get(current - 1);
    }

    private ParserError error(Token where, String message) {
        handler.error(where, message);
        return new ParserError();
    }

    private void synchronize() {
        advance();

        while (!atEnd()) {
            switch (previous().type) {
                case SEMICOLON:
                case LET:
                case FUNC:
                case CLASS:
                case IF:
                case ACTOR:
                case WHILE:
                case FOR:
                case RETURN:
                    return;
            }

            advance();
        }
    }
}
