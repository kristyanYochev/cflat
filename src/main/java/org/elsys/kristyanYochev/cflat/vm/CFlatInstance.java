package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.Token;

public interface CFlatInstance {
    CFlatValue get(Token name, CFlatContext context);
    void set(Token name, Object value, CFlatContext context);
    CFlatContext context();
}
