package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.*;
import org.elsys.kristyanYochev.cflat.ast.Stmt;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;

public class CFlatModule {
    private final Environment environment;
    private final String path;

    private CFlatModule(Environment environment, String path) {
        this.environment = environment;
        this.path = path;
    }

    public CFlatValue getValue(Token name) {
        CFlatValue theValue = this.environment.getAt(0, name.lexeme);
        if (theValue == null) {
            throw new Interpreter.RuntimeError(
                    "Cannot find value name '" + name.lexeme + "' in module '" + path + "'.",
                    name
            );
        }

        return theValue;
    }

    public void importAll(Environment target) {
        this.environment.copyValues(target);
    }

    private static final Map<String, CFlatModule> loadedModules = new HashMap<>();

    public static CFlatModule loadModule(String pathname, Interpreter interpreter, ErrorHandler handler) throws IOException {
        Path path = Paths.get(pathname);

        if (loadedModules.containsKey(path.toAbsolutePath().toString())) {
            return loadedModules.get(path.toAbsolutePath().toString());
        }

        String source = new String(Files.readAllBytes(path), Charset.defaultCharset());
        CFlatModule module = generateModule(pathname, interpreter, handler, source);

        if (module == null) return null;

        loadedModules.put(path.toAbsolutePath().toString(), module);

        return module;
    }

    public static CFlatModule loadModuleFromResources(String pathname, Interpreter interpreter, ErrorHandler handler) throws IOException {
        URL resourceUrl = CFlatModule.class.getResource(pathname);

        if (loadedModules.containsKey(resourceUrl.toString())) {
            return loadedModules.get(resourceUrl.toString());
        }

        String source = new String(
                CFlatModule.class.getResourceAsStream(pathname).readAllBytes(),
                Charset.defaultCharset()
        );
        CFlatModule module = generateModule(pathname, interpreter, handler, source);

        if (module == null) return null;

        loadedModules.put(resourceUrl.toString(), module);

        return module;
    }

    private static CFlatModule generateModule(String pathname, Interpreter interpreter, ErrorHandler handler, String source) {
        Lexer lexer = new Lexer(source, pathname, handler);

        List<Token> tokens = lexer.scanTokens();

        if (handler.hadError()) return null;

        Parser parser = new Parser(tokens, handler);
        List<Stmt> statements = parser.parse();

        if (handler.hadError()) return null;

        Resolver resolver = new Resolver(interpreter, handler);
        resolver.resolve(statements);

        if (handler.hadError()) return null;

        Environment moduleEnv = interpreter.executeModule(statements);

        if (handler.hadRuntimeError()) return null;

        return new CFlatModule(moduleEnv, pathname);
    }
}
