package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.Token;
import org.elsys.kristyanYochev.cflat.vm.builtins.CFlatNullClass;

public class CFlatValue {
    public final boolean mutable;
    private Object value;

    public CFlatValue(boolean mutable) {
        this(mutable, CFlatNullClass.theInstance);
    }

    public CFlatValue(boolean mutable, Object value) {
        this.mutable = mutable;
        this.value = value;
    }

    public Object value() {
        return value;
    }

    public void setValue(Object newValue, Token where) {
        if (mutable) {
            value = newValue;
            return;
        }

        throw new Interpreter.RuntimeError("Reassignment of immutable variable.", where);
    }

    public void setValueFromInitializer(Object newValue) {
        value = newValue;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
