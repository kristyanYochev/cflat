package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.Token;
import org.elsys.kristyanYochev.cflat.vm.builtins.*;

import java.util.*;
import java.util.stream.Collectors;

public class Environment {
    public final Environment parent;
    private final Map<String, CFlatValue> values = new HashMap<>();

    public Environment() {
        this(null);
    }

    public Environment(Environment parent) {
        this.parent = parent;
    }

    public void define(String name, CFlatValue value) {
        values.put(name, value);
    }

    public CFlatValue get(Token name) {
        if (values.containsKey(name.lexeme)) {
            return values.get(name.lexeme);
        }

        if (parent != null) return parent.get(name);

        throw new Interpreter.RuntimeError("Undefined variable '" + name.lexeme + "'.", name);
    }

    public CFlatValue getByName(String name) {
        if (values.containsKey(name)) {
            return values.get(name);
        }

        return null;
    }

    public CFlatValue getAt(int distance, String name) {
        CFlatValue value = ancestor(distance).values.get(name);
        assert value != null;
        return value;
    }

    public void assign(Token name, Object value) {
        if (values.containsKey(name.lexeme)) {
            values.get(name.lexeme).setValue(value, name);
            return;
        }

        if (parent != null) {
            parent.assign(name, value);
            return;
        }

        throw new Interpreter.RuntimeError("Undefined variable '" + name.lexeme + "'.", name);
    }

    public void assignAt(int distance, Token name, Object value) {
        ancestor(distance).values.get(name.lexeme).setValue(value, name);
    }

    public int depth() {
        int depth = 0;
        Environment env = this;
        for (; env.parent != null; depth++) {
            env = env.parent;
        }

        return depth;
    }

    private Environment ancestor(int distance) {
        Environment env = this;
        for (int i = 0; i < distance; i++) {
            env = env.parent;
        }

        return env;
    }

    public void copyValues(Environment target) {
        values.forEach(target::define);
    }

    @Override
    public String toString() {
        return values.entrySet().stream()
                .map(entry -> entry.getKey() + ": " + entry.getValue().toString())
                .collect(Collectors.joining("\n"));
    }

    private static Environment builtins() {
        Environment globalEnv = new Environment();

        globalEnv.define("print", new CFlatValue(false, new CFlatNativeFunction() {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                System.out.println(arguments.get(0).value());
                return new CFlatValue(false);
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        }));

        globalEnv.define("clock", new CFlatValue(false, new CFlatNativeFunction() {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                return new CFlatValue(
                        false,
                        new CFlatIntClass.CFlatInteger((int) System.currentTimeMillis())
                );
            }

            @Override
            public int argumentCount() {
                return 0;
            }
        }));

        globalEnv.define("stop", new CFlatValue(false, new CFlatNativeFunction() {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                Object argument = arguments.get(0).value();
                if (!(argument instanceof CFlatActorInstance)) {
                    throw new RuntimeException("Cannot stop a non-actor instance.");
                }

                CFlatActorInstance actorInstance = ((CFlatActorInstance) argument);

                actorInstance.stop();

                return new CFlatValue(false);
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        }));

        globalEnv.define("input", new CFlatValue(false, new CFlatNativeFunction() {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                Scanner scanner = new Scanner(System.in);
                return new CFlatValue(false, new CFlatStringClass.CFlatString(scanner.nextLine()));
            }

            @Override
            public int argumentCount() {
                return 0;
            }
        }));

        globalEnv.define("int", new CFlatValue(false, CFlatIntClass.theClass));
        globalEnv.define("float", new CFlatValue(false, CFlatFloatClass.theClass));
        globalEnv.define("bool", new CFlatValue(false, CFlatBoolClass.theClass));
        globalEnv.define("string", new CFlatValue(false, CFlatStringClass.theClass));

        return globalEnv;
    }

    public static Environment builtins = builtins();
}
