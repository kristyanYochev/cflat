package org.elsys.kristyanYochev.cflat.vm.builtins;

public interface CFlatNumber {
    double doubleValue();
}
