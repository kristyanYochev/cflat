package org.elsys.kristyanYochev.cflat.vm.builtins;

import org.elsys.kristyanYochev.cflat.vm.*;

import java.util.*;

public class CFlatBoolClass extends CFlatClass {
    public final static CFlatBoolClass theClass = new CFlatBoolClass();
    public final static CFlatBool TRUE = new CFlatBool(true);
    public final static CFlatBool FALSE = new CFlatBool(false);

    private CFlatBoolClass() {
        super("bool", null, generateMethods(), new ArrayList<>());
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        CFlatValue argumentValue = new CFlatValue(false, arguments.get(0).value());
        return new CFlatValue(false, javaBoolToCFlatBool(interpreter.isTruthy(argumentValue)));
    }

    @Override
    public int argumentCount() {
        return 1;
    }

    private static Map<String, Member<CFlatCallable>> generateMethods() {
        Map<String, Member<CFlatCallable>> methods = new HashMap<>();

        methods.put("__eq__", new Member<>(true, new CFlatNativeMethod<CFlatBool>(bool -> new CFlatNativeBoundMethod<>(bool) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatBool)) {
                    return new CFlatValue(false, FALSE);
                }

                CFlatBool otherBoolean = ((CFlatBool) other);

                return new CFlatValue(false, javaBoolToCFlatBool(
                        boundInstance.isTrue == otherBoolean.isTrue
                ));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__not__", new Member<>(true, new CFlatNativeMethod<CFlatBool>(bool -> new CFlatNativeBoundMethod<>(bool) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                return new CFlatValue(false, javaBoolToCFlatBool(!boundInstance.isTrue));
            }

            @Override
            public int argumentCount() {
                return 0;
            }
        })));

        return methods;
    }

    public static CFlatBool javaBoolToCFlatBool(boolean value) {
        return value ? TRUE : FALSE;
    }

    public static class CFlatBool extends CFlatClassInstance {
        public final boolean isTrue;

        private CFlatBool(boolean isTrue) {
            super(theClass);
            this.isTrue = isTrue;
        }

        @Override
        public String toString() {
            return String.valueOf(isTrue);
        }
    }
}
