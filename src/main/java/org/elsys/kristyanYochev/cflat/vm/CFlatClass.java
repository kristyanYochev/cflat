package org.elsys.kristyanYochev.cflat.vm;

import java.util.*;

public class CFlatClass implements CFlatCallable, CFlatContext {
    public static class Member<T> {
        public final boolean isPublic;
        public final T member;

        public Member(boolean isPublic, T member) {
            this.isPublic = isPublic;
            this.member = member;
        }
    }

    public static class Field {
        public final CFlatValue initialValue;
        public final String name;
        public final boolean mut;

        public Field(CFlatValue initialValue, String name, boolean mut) {
            this.initialValue = initialValue;
            this.name = name;
            this.mut = mut;
        }
    }

    public final String name;
    public final CFlatClass superclass;
    private final Map<String, Member<CFlatCallable>> methods;
    private final List<Member<Field>> fields;

    public CFlatClass(String name, CFlatClass superclass, Map<String, Member<CFlatCallable>> methods, List<Member<Field>> fields) {
        this.name = name;
        this.superclass = superclass;
        this.methods = methods;
        this.fields = fields;
    }

    public Member<CFlatCallable> findMethod(String name) {
        if (methods.containsKey(name)) {
            return methods.get(name);
        }

        if (superclass != null) {
            return superclass.findMethod(name);
        }

        return null;
    }

    public List<Member<Field>> fields() {
        List<Member<Field>> fieldList = new ArrayList<>(fields);

        if (superclass != null) {
            fieldList.addAll(superclass.fields());
        }

        return fieldList;
    }

    public boolean isChildOf(CFlatClass other) {
        if (this == other) return true;
        if (superclass == null) return false;

        return superclass.isChildOf(other);
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        CFlatClassInstance instance = new CFlatClassInstance(this);
        Member<CFlatCallable> initializer = findMethod("constructor");
        if (initializer != null) {
            try {
                interpreter.executeFunction(initializer.member.bind(instance), arguments);
            } finally {
                instance.finishInitialization();
            }
        }

        return new CFlatValue(false, instance);
    }

    @Override
    public int argumentCount() {
        Member<CFlatCallable> initializer = findMethod("constructor");
        return initializer == null ? 0 : initializer.member.argumentCount();
    }

    @Override
    public boolean mut() {
        return false;
    }

    @Override
    public CFlatContext binding() {
        return null;
    }

    @Override
    public CFlatCallable bind(CFlatInstance instance) {
        throw new RuntimeException("Binding a class to a instance.");
    }

    @Override
    public boolean allowsAccess(CFlatContext other) {
        if (!(other instanceof CFlatClass)) return false;

        return this.isChildOf((CFlatClass) other);
    }

    @Override
    public String toString() {
        return name;
    }
}
