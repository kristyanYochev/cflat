package org.elsys.kristyanYochev.cflat.vm.builtins;

import org.elsys.kristyanYochev.cflat.vm.*;

import java.util.*;

public class CFlatStringClass extends CFlatClass {
    public static final CFlatStringClass theClass = new CFlatStringClass();

    public CFlatStringClass() {
        super("string", null, generateMethods(), new ArrayList<>());
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        return new CFlatValue(false, new CFlatString(arguments.get(0).value().toString()));
    }

    @Override
    public int argumentCount() {
        return 1;
    }

    private static Map<String, Member<CFlatCallable>> generateMethods() {
        Map<String, Member<CFlatCallable>> methods = new HashMap<>();

        methods.put("__add__", new Member<>(true, new CFlatNativeMethod<CFlatString>(string -> new CFlatNativeBoundMethod<>(string) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatString)) {
                    throw new RuntimeException("Unsupported operation '+' on 'string'.");
                }

                CFlatString otherString = ((CFlatString) other);

                return new CFlatValue(false, new CFlatString(boundInstance.value + otherString.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__eq__", new Member<>(true, new CFlatNativeMethod<CFlatString>(string -> new CFlatNativeBoundMethod<>(string) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatString)) {
                    return new CFlatValue(false, CFlatBoolClass.FALSE);
                }

                CFlatString otherString = ((CFlatString) other);

                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(
                        boundInstance.value.equals(otherString.value)
                ));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        return methods;
    }

    public static class CFlatString extends CFlatClassInstance {
        private final String value;

        public CFlatString(String value) {
            super(theClass);
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
