package org.elsys.kristyanYochev.cflat.vm;

import java.util.*;
import java.util.function.*;

public class CFlatNativeMethod<InstanceType extends CFlatInstance> extends CFlatNativeFunction {
    private final Function<InstanceType, CFlatNativeBoundMethod<InstanceType>> boundMethodFactory;

    public CFlatNativeMethod(Function<InstanceType, CFlatNativeBoundMethod<InstanceType>> boundMethodFactory) {
        this.boundMethodFactory = boundMethodFactory;
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        throw new RuntimeException("Calling an unbound native method.");
    }

    @Override
    public int argumentCount() {
        return 0;
    }

    @Override
    public CFlatCallable bind(CFlatInstance instance) {
        // It'd be good if I find a way to assert that instance is of type InstanceType
        // but currently it's just an unchecked cast :(
        return boundMethodFactory.apply((InstanceType) instance);
    }
}
