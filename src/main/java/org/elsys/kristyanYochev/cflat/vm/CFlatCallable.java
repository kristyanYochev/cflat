package org.elsys.kristyanYochev.cflat.vm;

import java.util.List;

public interface CFlatCallable {
    CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments);
    int argumentCount();
    boolean mut();
    CFlatContext binding();
    CFlatCallable bind(CFlatInstance instance);
}
