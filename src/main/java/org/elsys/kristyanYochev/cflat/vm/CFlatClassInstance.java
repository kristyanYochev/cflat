package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.Token;
import org.elsys.kristyanYochev.cflat.vm.CFlatClass.Member;

import java.util.*;

public class CFlatClassInstance implements CFlatInstance {
    public final CFlatClass klass;
    private final Map<String, Member<CFlatValue>> fields = new HashMap<>();
    private boolean initializing = true;

    public CFlatClassInstance(CFlatClass klass) {
        this.klass = klass;
        klass.fields().forEach(fieldMember -> fields.put(
                fieldMember.member.name,
                new Member<>(
                        fieldMember.isPublic,
                        new CFlatValue(fieldMember.member.mut, fieldMember.member.initialValue.value())
                )
        ));
    }

    @Override
    public CFlatValue get(Token name, CFlatContext context) {
        if (fields.containsKey(name.lexeme)) {
            Member<CFlatValue> memberField = fields.get(name.lexeme);
            if (this.klass.allowsAccess(context) || memberField.isPublic) return memberField.member;
            else throw new Interpreter.RuntimeError(
                    "Access to private field '" + name.lexeme + "' on object of class '" + klass.name + "'.",
                    name
            );
        }

        Member<CFlatCallable> method = klass.findMethod(name.lexeme);
        if (method != null) {
            if (this.klass.allowsAccess(context) || method.isPublic) return new CFlatValue(false, method.member.bind(this));
            else throw new Interpreter.RuntimeError(
                    "Access to private method '" + name.lexeme + "' on object of class '" + klass.name + "'.",
                    name
            );
        }

        throw new Interpreter.RuntimeError("Undefined property '" + name.lexeme + "'.", name);
    }

    public CFlatCallable getBoundMethod(String name) {
        Member<CFlatCallable> method = klass.findMethod(name);
        return method != null ? method.member.bind(this) : null;
    }

    @Override
    public void set(Token name, Object value, CFlatContext context) {
        if (fields.containsKey(name.lexeme)) {
            Member<CFlatValue> memberField = fields.get(name.lexeme);
            if (this.klass.allowsAccess(context) || memberField.isPublic) {
                if (initializing) memberField.member.setValueFromInitializer(value);
                else memberField.member.setValue(value, name);

                return;
            }

            throw new Interpreter.RuntimeError(
                    "Access to private field '" + name.lexeme + "' on object of class '" + klass.name + "'.",
                    name
            );
        }

        throw new Interpreter.RuntimeError("Undefined property '" + name.lexeme + "'.", name);
    }

    @Override
    public CFlatContext context() {
        return klass;
    }

    public void finishInitialization() {
        initializing = false;
    }

    @Override
    public String toString() {
        return klass.name + " instance";
    }
}
