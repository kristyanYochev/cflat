package org.elsys.kristyanYochev.cflat.vm;

import java.util.*;

public class CFlatActor implements CFlatCallable, CFlatContext {
    public static class Field {
        public final boolean mut;
        public final String name;
        public final CFlatValue initialValue;

        public Field(boolean mut, String name, CFlatValue initialValue) {
            this.mut = mut;
            this.name = name;
            this.initialValue = initialValue;
        }
    }

    public final String name;
    private final Map<String, CFlatCallable> messages;
    private final Map<String, CFlatCallable> methods;
    private final List<Field> fields;

    public CFlatActor(String name, Map<String, CFlatCallable> messages, Map<String, CFlatCallable> methods, List<Field> fields) {
        this.name = name;
        this.messages = messages;
        this.methods = methods;
        this.fields = fields;
    }

    public CFlatCallable findMethod(String name) {
        if (methods.containsKey(name)) {
            return methods.get(name);
        }

        return null;
    }

    public CFlatCallable findMessage(String name) {
        if (messages.containsKey(name)) {
            return messages.get(name);
        }

        return null;
    }

    public List<Field> fields() {
        return fields;
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        Interpreter instanceInterpreter = new Interpreter(interpreter);
        CFlatActorInstance instance = new CFlatActorInstance(this, instanceInterpreter);
        CFlatCallable initializer = findMethod("constructor");

        if (initializer != null) {
            try {
                instanceInterpreter.executeFunction(initializer.bind(instance), arguments);
            } catch (Throwable ignored) {

            }
        }

        instance.finishInitialization();

        return new CFlatValue(false, instance);
    }

    @Override
    public int argumentCount() {
        CFlatCallable initializer = findMethod("constructor");
        return initializer != null ? initializer.argumentCount() : 0;
    }

    @Override
    public boolean mut() {
        return false;
    }

    @Override
    public CFlatContext binding() {
        return null;
    }

    @Override
    public CFlatCallable bind(CFlatInstance instance) {
        throw new RuntimeException("Binding an actor to an instance.");
    }

    @Override
    public boolean allowsAccess(CFlatContext other) {
        return this == other;
    }

    @Override
    public String toString() {
        return name;
    }
}
