package org.elsys.kristyanYochev.cflat.vm.builtins;

import org.elsys.kristyanYochev.cflat.vm.*;

import java.util.*;

public class CFlatIntClass extends CFlatClass {
    public static final CFlatIntClass theClass = new CFlatIntClass();

    private CFlatIntClass() {
        super("int", null, generateMethods(), new ArrayList<>());
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        Object toBeCast = arguments.get(0).value();

        if (toBeCast instanceof CFlatNumber) {
            double value = ((CFlatNumber) toBeCast).doubleValue();
            return new CFlatValue(
                    false,
                    new CFlatInteger((int) value)
            );
        }

        return new CFlatValue(
                false,
                new CFlatInteger(Integer.parseInt(toBeCast.toString()))
        );
    }

    @Override
    public int argumentCount() {
        return 1;
    }

    private static Map<String, Member<CFlatCallable>> generateMethods() {
        Map<String, Member<CFlatCallable>> methods = new HashMap<>();

        methods.put("__add__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    throw new RuntimeException("Right operand to '+' is not an integer.");
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                return new CFlatValue(false, new CFlatInteger(boundInstance.value + otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__sub__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    throw new RuntimeException("Right operand to '-' is not an integer.");
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                return new CFlatValue(false, new CFlatInteger(boundInstance.value - otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__mul__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    throw new RuntimeException("Right operand to '*' is not an integer.");
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                return new CFlatValue(false, new CFlatInteger(boundInstance.value * otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__div__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    throw new RuntimeException("Right operand to '/' is not an integer.");
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                if (otherInteger.value == 0) {
                    throw new RuntimeException("Division by zero.");
                }

                return new CFlatValue(false, new CFlatInteger(boundInstance.value / otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__mod__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    throw new RuntimeException("Right operand to '%' is not an integer.");
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                if (otherInteger.value == 0) {
                    throw new RuntimeException("Division by zero.");
                }

                return new CFlatValue(false, new CFlatInteger(boundInstance.value % otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__comp__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    throw new RuntimeException("Comparing an integer to a non-integer.");
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                return new CFlatValue(false, new CFlatInteger(boundInstance.value - otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__eq__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatInteger)) {
                    return new CFlatValue(false, CFlatBoolClass.FALSE);
                }

                CFlatInteger otherInteger = ((CFlatInteger) other);

                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(
                        boundInstance.value == otherInteger.value
                ));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__neg__", new Member<>(true, new CFlatNativeMethod<CFlatInteger>(integer -> new CFlatNativeBoundMethod<>(integer) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                return new CFlatValue(false, new CFlatInteger(-boundInstance.value));
            }

            @Override
            public int argumentCount() {
                return 0;
            }
        })));

        return methods;
    }

    public static class CFlatInteger extends CFlatClassInstance implements CFlatNumber {
        public final int value;

        public CFlatInteger(int value) {
            super(theClass);
            this.value = value;
        }

        @Override
        public double doubleValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}
