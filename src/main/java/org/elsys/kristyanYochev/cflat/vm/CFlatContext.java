package org.elsys.kristyanYochev.cflat.vm;

public interface CFlatContext {
    boolean allowsAccess(CFlatContext other);
}
