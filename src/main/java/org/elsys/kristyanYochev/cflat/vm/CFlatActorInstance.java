package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.Token;

import java.util.*;
import java.util.concurrent.*;

public class CFlatActorInstance implements CFlatInstance, Runnable {
    public static class MessageDescription {
        public final boolean mut;
        public final int argumentCount;

        public MessageDescription(boolean mut, int argumentCount) {
            this.mut = mut;
            this.argumentCount = argumentCount;
        }
    }

    public final CFlatActor actor;
    private final Map<String, CFlatValue> fields = new HashMap<>();
    private final BlockingQueue<CFlatMessage> messagesToProcess = new LinkedBlockingQueue<>();
    private final Interpreter interpreter;
    private boolean initializing = true;
    private Thread thread;

    public CFlatActorInstance(CFlatActor actor, Interpreter interpreter) {
        this.actor = actor;
        this.interpreter = interpreter;
        actor.fields().forEach(field -> fields.put(
                field.name,
                new CFlatValue(field.mut, field.initialValue.value()))
        );
        this.thread = null;
    }

    public void finishInitialization() {
        initializing = false;
        thread = new Thread(this);
        thread.start();
    }

    public MessageDescription getMessageDescription(String messageName) {
        CFlatCallable messageCallable = actor.findMessage(messageName);

        return messageCallable == null ? null : new MessageDescription(
                messageCallable.mut(),
                messageCallable.argumentCount()
        );
    }

    public void sendMessage(CFlatMessage message) {
        try {
            messagesToProcess.put(message);
        } catch (InterruptedException e) {
            sendMessage(message);
        }
    }

    public void stop() {
        thread.interrupt();
    }

    public CFlatCallable getMainMethod() {
        CFlatCallable method = actor.findMethod("main");
        return method != null ? method.bind(this) : null;
    }

    @Override
    public void run() {
        try {
            //noinspection InfiniteLoopStatement
            while (true) {
                CFlatMessage message = messagesToProcess.take();

                CFlatCallable callable = actor.findMessage(message.name).bind(this);

                try {
                    interpreter.executeFunction(callable, message.arguments);
                } catch (Interpreter.RuntimeError error) {
                    interpreter.reportError(error);
                }
            }
        } catch (InterruptedException ignored) {
            // Stop the thread
        }
    }

    @Override
    public CFlatValue get(Token name, CFlatContext context) {
        if (!actor.allowsAccess(context))
            throw new Interpreter.RuntimeError("Accessing field or method on an actor", name);

        if (fields.containsKey(name.lexeme)) {
            return fields.get(name.lexeme);
        }

        CFlatCallable method = actor.findMethod(name.lexeme);
        if (method != null) {
            return new CFlatValue(false, method.bind(this));
        }

        throw new Interpreter.RuntimeError("Undefined property '" + name.lexeme + "'.", name);
    }

    @Override
    public void set(Token name, Object value, CFlatContext context) {
        if (actor.allowsAccess(context)) {
            if (initializing) fields.get(name.lexeme).setValueFromInitializer(value);
            else fields.get(name.lexeme).setValue(value, name);
        }
    }

    @Override
    public CFlatContext context() {
        return actor;
    }

    @Override
    public String toString() {
        return "<" + actor.name + " instance>";
    }
}
