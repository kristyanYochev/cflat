package org.elsys.kristyanYochev.cflat.vm;

public abstract class CFlatNativeBoundMethod<Instance extends CFlatInstance> extends CFlatNativeFunction {
    protected final Instance boundInstance;

    public CFlatNativeBoundMethod(Instance boundInstance) {
        this.boundInstance = boundInstance;
    }

    @Override
    public CFlatCallable bind(CFlatInstance instance) {
        throw new RuntimeException("Method is already bound.");
    }

    @Override
    public CFlatContext binding() {
        return boundInstance.context();
    }
}
