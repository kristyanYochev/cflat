package org.elsys.kristyanYochev.cflat.vm.builtins;

import org.elsys.kristyanYochev.cflat.vm.*;

import java.util.*;

public class CFlatFloatClass extends CFlatClass {
    public static final CFlatFloatClass theClass = new CFlatFloatClass();

    private CFlatFloatClass() {
        super("float", null, generateMethods(), new ArrayList<>());
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        Object toBeCast = arguments.get(0).value();

        if (toBeCast instanceof CFlatNumber) {
            double value = ((CFlatNumber) toBeCast).doubleValue();
            return new CFlatValue(
                    false,
                    new CFlatFloat((float) value)
            );
        }

        return new CFlatValue(
                false,
                new CFlatFloat(Float.parseFloat(toBeCast.toString()))
        );
    }

    private static Map<String, Member<CFlatCallable>> generateMethods() {
        Map<String, Member<CFlatCallable>> methods = new HashMap<>();

        methods.put("__add__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatFloat)) {
                    throw new RuntimeException("Right operand to '+' is not an integer.");
                }

                CFlatFloat otherInteger = ((CFlatFloat) other);

                return new CFlatValue(false, new CFlatFloat(boundInstance.value + otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__sub__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatFloat)) {
                    throw new RuntimeException("Right operand to '-' is not an integer.");
                }

                CFlatFloat otherInteger = ((CFlatFloat) other);

                return new CFlatValue(false, new CFlatFloat(boundInstance.value - otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__mul__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatFloat)) {
                    throw new RuntimeException("Right operand to '*' is not an integer.");
                }

                CFlatFloat otherInteger = ((CFlatFloat) other);

                return new CFlatValue(false, new CFlatFloat(boundInstance.value * otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__div__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatFloat)) {
                    throw new RuntimeException("Right operand to '/' is not an integer.");
                }

                CFlatFloat otherInteger = ((CFlatFloat) other);

                if (otherInteger.value == 0) {
                    throw new RuntimeException("Division by zero.");
                }

                return new CFlatValue(false, new CFlatFloat(boundInstance.value / otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__comp__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatFloat)) {
                    throw new RuntimeException("Comparing an integer to a non-integer.");
                }

                CFlatFloat otherInteger = ((CFlatFloat) other);

                return new CFlatValue(false, new CFlatFloat(boundInstance.value - otherInteger.value));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__eq__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                Object other = arguments.get(0).value();
                if (!(other instanceof CFlatFloat)) {
                    return new CFlatValue(false, CFlatBoolClass.FALSE);
                }

                CFlatFloat otherFloat = ((CFlatFloat) other);

                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(
                        boundInstance.value == otherFloat.value
                ));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        methods.put("__neg__", new Member<>(true, new CFlatNativeMethod<CFlatFloat>(cFlatFloat -> new CFlatNativeBoundMethod<>(cFlatFloat) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance != null;
                assert boundInstance.klass == theClass;

                return new CFlatValue(false, new CFlatFloat(-boundInstance.value));
            }

            @Override
            public int argumentCount() {
                return 0;
            }
        })));

        return methods;
    }

    public static class CFlatFloat extends CFlatClassInstance implements CFlatNumber {
        public final float value;

        public CFlatFloat(float value) {
            super(theClass);
            this.value = value;
        }

        @Override
        public double doubleValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}
