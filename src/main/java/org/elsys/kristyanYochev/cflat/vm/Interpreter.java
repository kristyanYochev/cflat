package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.*;
import org.elsys.kristyanYochev.cflat.ast.*;
import org.elsys.kristyanYochev.cflat.vm.builtins.*;
import org.elsys.kristyanYochev.cflat.vm.builtins.CFlatBoolClass.CFlatBool;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Interpreter implements Stmt.Visitor<Void>, Expr.Visitor<CFlatValue> {
    private static final List<TokenType> comparisonOperators = List.of(
            TokenType.GREATER_EQUAL,
            TokenType.GREATER,
            TokenType.LESS,
            TokenType.LESS_EQUAL
    );

    private static final List<TokenType> equalityOperators = List.of(TokenType.EQUAL_EQUAL, TokenType.BANG_EQUAL);

    public static class RuntimeError extends RuntimeException {
        public final Token token;

        public RuntimeError(String message, Token token) {
            super(message);
            this.token = token;
        }
    }

    public static class Return extends RuntimeException {
        public final CFlatValue value;

        public Return(CFlatValue value) {
            this.value = value;
        }
    }

    private static class FunctionDescription {
        public final boolean mut;
        public final int depth;
        public final CFlatContext context;

        public FunctionDescription(boolean mut, int depth, CFlatContext context) {
            this.mut = mut;
            this.depth = depth;
            this.context = context;
        }
    }

    private static final Map<TokenType, String> binaryOperatorFunctionNames = new HashMap<>();
    private static final Map<TokenType, String> unaryOperatorFunctionNames = new HashMap<>();

    static {
        binaryOperatorFunctionNames.put(TokenType.PLUS, "__add__");
        binaryOperatorFunctionNames.put(TokenType.MINUS, "__sub__");
        binaryOperatorFunctionNames.put(TokenType.STAR, "__mul__");
        binaryOperatorFunctionNames.put(TokenType.SLASH, "__div__");
        binaryOperatorFunctionNames.put(TokenType.MODULO, "__mod__");

        binaryOperatorFunctionNames.put(TokenType.GREATER, "__comp__");
        binaryOperatorFunctionNames.put(TokenType.GREATER_EQUAL, "__comp__");
        binaryOperatorFunctionNames.put(TokenType.LESS, "__comp__");
        binaryOperatorFunctionNames.put(TokenType.LESS_EQUAL, "__comp__");

        binaryOperatorFunctionNames.put(TokenType.EQUAL_EQUAL, "__eq__");
        binaryOperatorFunctionNames.put(TokenType.BANG_EQUAL, "__eq__");
    }

    static {
        unaryOperatorFunctionNames.put(TokenType.MINUS, "__neg__");
        unaryOperatorFunctionNames.put(TokenType.BANG, "__not__");
    }

    private final ErrorHandler handler;
    public final Environment globals;
    private final Map<Expr, Integer> locals;

    private Environment environment;
    private FunctionDescription currentFunction = null;

    public Interpreter(ErrorHandler handler) {
        this.handler = handler;

        this.globals = new Environment(Environment.builtins);
        this.environment = this.globals;
        this.locals = new HashMap<>();
    }

    public Interpreter(Interpreter interpreter) {
        this.handler = interpreter.handler;
        this.locals = interpreter.locals;
        this.globals = interpreter.globals;
        this.environment = interpreter.globals;
        this.currentFunction = null;
    }

    public CFlatValue eval(Expr expr) {
        return expr.accept(this);
    }

    public void interpret(List<Stmt> statements) {
        try {
            CFlatModule standardLibrary = null;
            try {
                standardLibrary = CFlatModule.loadModuleFromResources("/stdlib.cf", this, handler);
            } catch (IOException ex) {
                System.err.println("Unable to load standard library.");
                System.exit(CFlat.RUNTIME_ERROR_CODE);
            }

            assert standardLibrary != null;
            standardLibrary.importAll(environment);

            statements.forEach(this::execute);

            CFlatValue main = environment.getByName("Main");
            if (!(main.value() instanceof CFlatActor)) {
                System.err.println("No main actor found.");
                return;
            }

            CFlatActor mainActor = ((CFlatActor) main.value());
            CFlatActorInstance mainInstance = new CFlatActorInstance(mainActor, new Interpreter(this));
            mainInstance.finishInitialization();

            CFlatCallable mainMethod = mainInstance.getMainMethod();
            if (mainMethod == null) {
                System.err.println("No main method found.");
                return;
            }

            executeFunction(mainMethod, new ArrayList<>());
        } catch (RuntimeError error) {
            handler.runtimeError(error);
        }
    }

    public void execute(Stmt stmt) {
        stmt.accept(this);
    }

    public void executeBlock(List<Stmt> block, Environment environment) {
        Environment previous = this.environment;

        try {
            this.environment = environment;
            block.forEach(this::execute);
        } finally {
            this.environment = previous;
        }
    }

    public Environment executeModule(List<Stmt> statements) {
        Environment moduleEnvironment = new Environment(Environment.builtins);
        executeBlock(statements, moduleEnvironment);
        return moduleEnvironment;
    }

    public CFlatValue executeFunction(CFlatCallable callable, List<CFlatValue> arguments, Token callPoint) {
        try {
            return executeFunction(callable, arguments);
        } catch (RuntimeException ex) {
            if (ex instanceof NullPointerException) throw ex;

            throw new RuntimeError(ex.getMessage(), callPoint);
        }
    }

    public CFlatValue executeFunction(CFlatCallable callable, List<CFlatValue> arguments) {
        if (arguments.size() != callable.argumentCount()) {
            throw new RuntimeException("Expected " + callable.argumentCount() + " arguments, got " + arguments.size() + ".");
        }

        FunctionDescription caller = currentFunction;

        currentFunction = new FunctionDescription(callable.mut(), environment.depth(), callable.binding());
        CFlatValue returnValue = callable.call(this, arguments);
        currentFunction = caller;

        return returnValue;
    }

    @Override
    public CFlatValue visitBinaryExpr(Expr.Binary binary) {
        Object leftValue = eval(binary.left).value();
        Object rightValue = eval(binary.right).value();

        if (!(leftValue instanceof CFlatClassInstance)) {
            throw new RuntimeError("Left value is not an instance.", binary.operator);
        }

        String dataModelFunctionName = binaryOperatorFunctionNames.get(binary.operator.type);

        if (dataModelFunctionName == null) {
            throw new RuntimeError("Unknown operator: '" + binary.operator.lexeme + "'.", binary.operator);
        }

        CFlatCallable operatorFunction = ((CFlatClassInstance)leftValue).getBoundMethod(dataModelFunctionName);

        if (operatorFunction == null) {
            throw new RuntimeError(
                    "Operand of type '" + ((CFlatClassInstance) leftValue).klass +
                            "' does not support operator '" + binary.operator.lexeme + "'.",
                    binary.operator
            );
        }

        CFlatValue result = executeFunction(
                operatorFunction,
                Collections.singletonList(new CFlatValue(false, rightValue)),
                binary.operator
        );

        if (comparisonOperators.contains(binary.operator.type) && !(result.value() instanceof CFlatNumber)) {
            throw new RuntimeError("Expect __comp__ to return a number.", binary.operator);
        }

        if (equalityOperators.contains(binary.operator.type) && !(result.value() instanceof CFlatBool)) {
            throw new RuntimeError("Expect __eq__ to return a bool.", binary.operator);
        }

        switch (binary.operator.type) {
            case GREATER_EQUAL: {
                assert result.value() instanceof CFlatNumber;
                double comparison = ((CFlatNumber) result.value()).doubleValue();
                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(comparison >= 0));
            }
            case GREATER: {
                assert result.value() instanceof CFlatNumber;
                double comparison = ((CFlatNumber) result.value()).doubleValue();
                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(comparison > 0));
            }
            case LESS_EQUAL: {
                assert result.value() instanceof CFlatNumber;
                double comparison = ((CFlatNumber) result.value()).doubleValue();
                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(comparison <= 0));
            }
            case LESS: {
                assert result.value() instanceof CFlatNumber;
                double comparison = ((CFlatNumber) result.value()).doubleValue();
                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(comparison < 0));
            }
            case BANG_EQUAL: {
                assert result.value() instanceof CFlatBool;
                boolean equal = ((CFlatBool) result.value()).isTrue;
                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(!equal));
            }
        }

        return result;
    }

    @Override
    public CFlatValue visitLiteralExpr(Expr.Literal literal) {
        return new CFlatValue(false, literal.value);
    }

    @Override
    public CFlatValue visitGroupingExpr(Expr.Grouping grouping) {
        return eval(grouping.expression);
    }

    @Override
    public CFlatValue visitUnaryExpr(Expr.Unary unary) {
        Object rightValue = eval(unary.right).value();

        if (!(rightValue instanceof CFlatClassInstance)) {
            throw new RuntimeError(
                    "Operand to '" + unary.operator.lexeme + "' is not an instance.",
                    unary.operator
            );
        }

        CFlatClassInstance operand = ((CFlatClassInstance)rightValue);

        String dataModelFunctionName = unaryOperatorFunctionNames.get(unary.operator.type);

        if (dataModelFunctionName == null) {
            throw new RuntimeError("Unknown operator: '" + unary.operator.lexeme + "'.", unary.operator);
        }

        CFlatCallable function = operand.getBoundMethod(dataModelFunctionName);

        if (function == null) {
            throw new RuntimeError(
                    "Operand of type '" + operand.klass.name +
                            "' does not support unary '" + unary.operator.lexeme + "'.",
                    unary.operator
            );
        }

        return executeFunction(function, new ArrayList<>(), unary.operator);
    }

    @Override
    public CFlatValue visitLogicalExpr(Expr.Logical logical) {
        boolean leftValue = isTruthy(eval(logical.left));

        if (logical.operator.type == TokenType.OR) {
            if (leftValue) return new CFlatValue(false, true);
        } else if (logical.operator.type == TokenType.AND) {
            if (!leftValue) return new CFlatValue(false, false);
        }

        return eval(logical.right);
    }

    @Override
    public CFlatValue visitReferenceExpr(Expr.Reference reference) {
        return lookUpVariable(reference.name, reference);
    }

    @Override
    public CFlatValue visitCallExpr(Expr.Call call) {
        CFlatValue callee = eval(call.callee);

        if (!(callee.value() instanceof CFlatCallable)) {
            throw new RuntimeError("Can only call functions.", call.paren);
        }

        CFlatCallable callable = (CFlatCallable) callee.value();

        if (currentFunction != null && !currentFunction.mut && callable.mut()) {
            throw new RuntimeError(
                    "Calling mutator function from non-mut context.",
                    call.paren
            );
        }

        List<CFlatValue> arguments = call.arguments.stream()
                .map(this::eval)
                .collect(Collectors.toList());

        return executeFunction(callable, arguments, call.paren);
    }

    @Override
    public CFlatValue visitAssignmentExpr(Expr.Assignment assignment) {
        CFlatValue value = eval(assignment.value);

        Integer distance = locals.get(assignment);
        if (distance != null) {
            assert currentFunction.mut || distance < environment.depth() - currentFunction.depth;
            environment.assignAt(distance, assignment.name, value.value());
        } else {
            if (currentFunction == null || currentFunction.mut) {
                globals.assign(assignment.name, value.value());
            } else {
                throw new RuntimeError("Non-mut function mutating non-local state!", assignment.name);
            }
        }
        return value;
    }

    @Override
    public CFlatValue visitGetExpr(Expr.Get get) {
        CFlatValue object = eval(get.object);
        if (object.value() instanceof CFlatInstance) {
            CFlatInstance instance = (CFlatInstance) object.value();
            CFlatValue getValue = instance.get(get.name, currentContext());

            return new CFlatValue(object.mutable && getValue.mutable, getValue.value());
        }

        throw new RuntimeError("Only instances have properties.", get.name);
    }

    @Override
    public CFlatValue visitSetExpr(Expr.Set set) {
        CFlatValue object = eval(set.object);

        if (!(object.value() instanceof CFlatInstance)) {
            throw new RuntimeError("Only instances have fields.", set.name);
        }

        if (!object.mutable) {
            throw new RuntimeError("Trying to mutate an immutable object.", set.name);
        }

        CFlatValue value = eval(set.newValue);
        ((CFlatInstance) object.value()).set(set.name, value.value(), currentContext());
        return value;
    }

    @Override
    public CFlatValue visitThisExpr(Expr.This thisExpr) {
        return lookUpVariable(thisExpr.keyword, thisExpr);
    }

    @Override
    public CFlatValue visitSuperExpr(Expr.Super superExpr) {
        int distance = locals.get(superExpr);
        CFlatClass superclass = (CFlatClass) environment.getAt(distance, "super").value();
        CFlatClassInstance object = (CFlatClassInstance) environment.getAt(distance - 1, "this").value();

        CFlatClass.Member<CFlatCallable> methodMember = superclass.findMethod(superExpr.name.lexeme);

        if (methodMember == null) {
            throw new RuntimeError("Undefined property '" + superExpr.name.lexeme + "'.", superExpr.name);
        }

        CFlatContext context = currentContext();
        if (methodMember.isPublic || (context != null && context.allowsAccess(superclass))) {
            CFlatCallable method = methodMember.member;
            return new CFlatValue(false, method.bind(object));
        }

        throw new RuntimeError("Access to private property '" + superExpr.name.lexeme + "'.", superExpr.name);
    }

    @Override
    public CFlatValue visitMemberCallExpr(Expr.MemberCall memberCall) {
        CFlatValue receiverValue = eval(memberCall.object);

        if (!(receiverValue.value() instanceof CFlatInstance)) {
            throw new RuntimeError("Only instances have properties.", memberCall.memberName);
        }

        CFlatInstance receiver = (CFlatInstance) receiverValue.value();

        CFlatValue calleeValue = receiver.get(memberCall.memberName, currentContext());

        if (!(calleeValue.value() instanceof CFlatCallable)) {
            throw new RuntimeError("Can only call functions.", memberCall.memberName);
        }

        CFlatCallable callable = ((CFlatCallable) calleeValue.value());

        if (!receiverValue.mutable && callable.mut()) {
            throw new RuntimeError("Calling mutator function on an immutable object.", memberCall.memberName);
        }

        if (currentFunction != null && !currentFunction.mut && callable.mut()) {
            throw new RuntimeError(
                    "Calling mutator function from non-mut context.",
                    memberCall.memberName
            );
        }

        List<CFlatValue> arguments = memberCall.arguments.stream()
                .map(this::eval)
                .collect(Collectors.toList());

        return executeFunction(callable, arguments, memberCall.memberName);
    }

    @Override
    public CFlatValue visitLambdaExpr(Expr.Lambda lambda) {
        CFlatFunction function = new CFlatFunction(lambda, environment);
        return new CFlatValue(false, function);
    }

    @Override
    public CFlatValue visitPartialExpr(Expr.Partial partial) {
        Object calleeObj = eval(partial.callee).value();

        if (!(calleeObj instanceof CFlatCallable)) {
            throw new RuntimeError("Partially applying a non-callable object.", partial.keyword);
        }

        CFlatCallable callable = ((CFlatCallable) calleeObj);

        Map<Integer, CFlatValue> prepassedArgs = new HashMap<>();

        if (callable.argumentCount() != partial.arguments.size()) {
            throw new RuntimeError(
                    "Expected " + callable.argumentCount() +
                            " arguments for partial, got " + partial.arguments.size() + ".",
                    partial.keyword
            );
        }

        for (int argIndex = 0; argIndex < partial.arguments.size(); argIndex++) {
            Expr argumentExpr = partial.arguments.get(argIndex);
            if (argumentExpr == null) continue;
            prepassedArgs.put(argIndex, eval(argumentExpr));
        }

        CFlatCallable resultFn = new CFlatPartialFunction(callable, prepassedArgs, currentContext());

        return new CFlatValue(false, resultFn);
    }

    @Override
    public Void visitExpressionStatement(Stmt.Expression expression) {
        eval(expression.expression);
        return null;
    }

    @Override
    public Void visitVarDeclaration(Stmt.VarDeclaration varDeclaration) {
        CFlatValue value = new CFlatValue(varDeclaration.mut);
        if (varDeclaration.initialValue != null) {
            value = new CFlatValue(varDeclaration.mut, eval(varDeclaration.initialValue).value());
        }

        environment.define(varDeclaration.name.lexeme, value);
        return null;
    }

    @Override
    public Void visitFunctionDeclaration(Stmt.FunctionDeclaration functionDeclaration) {
        CFlatFunction function = new CFlatFunction(functionDeclaration, environment);
        environment.define(functionDeclaration.name.lexeme, new CFlatValue(false, function));

        return null;
    }

    @Override
    public Void visitReturnStatement(Stmt.ReturnStatement returnStatement) {
        CFlatValue value = returnStatement.returnValue == null ?
                new CFlatValue(false) :
                eval(returnStatement.returnValue);

        throw new Return(value);
    }

    @Override
    public Void visitBlock(Stmt.Block block) {
        executeBlock(block.statements, new Environment(environment));
        return null;
    }

    @Override
    public Void visitIfStatement(Stmt.IfStatement ifStatement) {
        if (isTruthy(eval(ifStatement.condition))) {
            execute(ifStatement.thenBlock);
        } else if (ifStatement.elseBlock != null) {
            execute(ifStatement.elseBlock);
        }
        return null;
    }

    @Override
    public Void visitWhile(Stmt.While whileStatement) {
        while (isTruthy(eval(whileStatement.condition))) {
            execute(whileStatement.body);
        }
        return null;
    }

    @Override
    public Void visitClassDecl(Stmt.ClassDecl classDecl) {
        Object superclass = null;
        if (classDecl.superclass != null) {
            superclass = eval(classDecl.superclass).value();
            if (!(superclass instanceof CFlatClass)) {
                throw new RuntimeError("Superclass must be a class.", classDecl.superclass.name);
            }
        }

        environment.define(classDecl.name.lexeme, new CFlatValue(false));

        if (classDecl.superclass != null) {
            environment = new Environment(environment);
            environment.define("super", new CFlatValue(false, superclass));
        }

        Map<String, CFlatClass.Member<CFlatCallable>> methods = new HashMap<>();
        classDecl.methods.forEach(methodMember -> {
            Stmt.FunctionDeclaration method = methodMember.member;
            CFlatFunction function = new CFlatFunction(method, environment);
            methods.put(method.name.lexeme, new CFlatClass.Member<>(methodMember.isPublic, function));
        });

        List<CFlatClass.Member<CFlatClass.Field>> fields =
                classDecl.fields.stream().map(field ->
                    new CFlatClass.Member<>(
                            field.isPublic,
                            new CFlatClass.Field(
                                    field.member.initialValue == null ? new CFlatValue(false) : eval(field.member.initialValue),
                                    field.member.name.lexeme,
                                    field.member.mut
                            )
                    )
        ).collect(Collectors.toList());

        CFlatClass klass = new CFlatClass(classDecl.name.lexeme, (CFlatClass) superclass, methods, fields);

        if (superclass != null) {
            environment = environment.parent;
        }

        environment.define(classDecl.name.lexeme, new CFlatValue(false, klass));
        return null;
    }

    @Override
    public Void visitActorDecl(Stmt.ActorDecl actorDecl) {
        environment.define(actorDecl.name.lexeme, new CFlatValue(false));

        Map<String, CFlatCallable> methods = new HashMap<>();
        actorDecl.methods.forEach(method -> {
            CFlatFunction function = new CFlatFunction(method, environment);
            methods.put(method.name.lexeme, function);
        });

        Map<String, CFlatCallable> messages = new HashMap<>();
        actorDecl.messages.forEach(message -> {
            CFlatFunction function = new CFlatFunction(message, environment);
            messages.put(message.name.lexeme, function);
        });

        List<CFlatActor.Field> fields =
                actorDecl.fields.stream().map(field ->
                        new CFlatActor.Field(
                                field.mut,
                                field.name.lexeme,
                                field.initialValue == null ? new CFlatValue(false) : eval(field.initialValue)
                        )
        ).collect(Collectors.toList());

        CFlatActor actor = new CFlatActor(actorDecl.name.lexeme, messages, methods, fields);

        environment.define(actorDecl.name.lexeme, new CFlatValue(false, actor));

        return null;
    }

    @Override
    public Void visitSignal(Stmt.Signal signal) {
        CFlatValue receiver = eval(signal.actorInstance);

        if (!(receiver.value() instanceof CFlatActorInstance)) {
            throw new RuntimeError("Receiver of message is not an actor.", signal.messageName);
        }

        CFlatActorInstance receiverActor = ((CFlatActorInstance) receiver.value());

        CFlatActorInstance.MessageDescription messageDescription =
                receiverActor.getMessageDescription(signal.messageName.lexeme);

        if (messageDescription == null)
            throw new RuntimeError(
                    "No message '" + signal.messageName.lexeme +
                        "' on actor instance of type '" + receiverActor.actor.name + "'.",
                    signal.messageName
            );

        if (!receiver.mutable && messageDescription.mut)
            throw new RuntimeError("Sending mut message on an immutable actor instance.", signal.messageName);

        if (messageDescription.argumentCount != signal.arguments.size())
            throw new RuntimeError(
                    "Expected " + messageDescription.argumentCount +
                            " arguments but got " + signal.arguments.size(),
                    signal.messageName
            );

        List<CFlatValue> argumentValues = signal.arguments.stream().map(this::eval).collect(Collectors.toList());

        receiverActor.sendMessage(new CFlatMessage(signal.messageName.lexeme, argumentValues));

        return null;
    }

    @Override
    public Void visitImport(Stmt.Import importStmt) {
        String modulePath = importStmt.modulePath.lexeme;
        modulePath = modulePath.substring(1, modulePath.length() - 1);

        CFlatModule module;
        try {
            module = CFlatModule.loadModule(modulePath, this, handler);
        } catch (IOException exception) {
            throw new RuntimeError("Unable to find module '" + modulePath + "'.", importStmt.modulePath);
        }

        if (module == null) throw new RuntimeError("Errors occurred while loading module.", importStmt.modulePath);

        importStmt.names.forEach(name -> environment.define(name.lexeme, module.getValue(name)));

        return null;
    }

    private CFlatContext currentContext() {
        return currentFunction == null ? null : currentFunction.context;
    }

    public boolean isTruthy(CFlatValue value) {
        if (value.value() == null) return false;
        if (value.value() == CFlatNullClass.theInstance) return false;
        if (value.value() instanceof CFlatBool) return ((CFlatBool) value.value()).isTrue;
        return true;
    }

    public void resolve(Expr expr, int distance) {
        locals.put(expr, distance);
    }

    public void reportError(RuntimeError error) {
        handler.runtimeError(error);
    }

    private CFlatValue lookUpVariable(Token name, Expr expr) {
        Integer distance = locals.get(expr);
        return distance != null ?
                environment.getAt(distance, name.lexeme) :
                globals.get(name);
    }
}
