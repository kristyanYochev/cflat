package org.elsys.kristyanYochev.cflat.vm;

public abstract class CFlatNativeFunction implements CFlatCallable {
    @Override
    public String toString() {
        return "<native fn>";
    }

    @Override
    public boolean mut() {
        return false;
    }

    @Override
    public CFlatContext binding() {
        return null;
    }

    @Override
    public CFlatCallable bind(CFlatInstance instance) {
        throw new RuntimeException("Binding a builtin function.");
    }
}
