package org.elsys.kristyanYochev.cflat.vm;

import java.util.*;

public class CFlatPartialFunction implements CFlatCallable {
    private final CFlatCallable callable;
    private final Map<Integer, CFlatValue> prepassedArgs;
    private final CFlatContext context;

    public CFlatPartialFunction(CFlatCallable callable, Map<Integer, CFlatValue> prepassedArgs, CFlatContext context) {
        this.callable = callable;
        this.prepassedArgs = prepassedArgs;
        this.context = context;
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        List<CFlatValue> actualArgs = new ArrayList<>();
        int passedArgIndex = 0;
        for (int argIndex = 0; argIndex < callable.argumentCount(); argIndex++) {
            if (prepassedArgs.containsKey(argIndex)) {
                actualArgs.add(argIndex, prepassedArgs.get(argIndex));
            } else {
                actualArgs.add(argIndex, arguments.get(passedArgIndex));
                passedArgIndex++;
            }
        }

        return callable.call(interpreter, actualArgs);
    }

    @Override
    public int argumentCount() {
        return callable.argumentCount() - prepassedArgs.size();
    }

    @Override
    public boolean mut() {
        return callable.mut();
    }

    @Override
    public CFlatContext binding() {
        return context;
    }

    @Override
    public CFlatCallable bind(CFlatInstance instance) {
        throw new RuntimeException("Binding a partial function to an object!");
    }
}
