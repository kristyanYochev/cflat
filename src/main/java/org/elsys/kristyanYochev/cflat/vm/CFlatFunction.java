package org.elsys.kristyanYochev.cflat.vm;

import org.elsys.kristyanYochev.cflat.ast.Expr;
import org.elsys.kristyanYochev.cflat.ast.Stmt;
import org.elsys.kristyanYochev.cflat.vm.builtins.CFlatNullClass;

import java.util.List;

public class CFlatFunction implements CFlatCallable {
    private final Stmt.FunctionDeclaration declaration;
    private final Environment closure;
    private final CFlatContext binding;

    public CFlatFunction(Stmt.FunctionDeclaration declaration, Environment closure, CFlatContext binding) {
        this.declaration = declaration;
        this.closure = closure;
        this.binding = binding;
    }

    public CFlatFunction(Stmt.FunctionDeclaration declaration, Environment closure) {
        this(declaration, closure, null);
    }

    public CFlatFunction(Expr.Lambda lambda, Environment closure) {
        this(
                new Stmt.FunctionDeclaration(
                        null,
                        lambda.arguments,
                        lambda.body,
                        lambda.mut
                ),
                closure
        );
    }

    @Override
    public CFlatFunction bind(CFlatInstance instance) {
        Environment env = new Environment(closure);
        env.define("this", new CFlatValue(mut(), instance));
        return new CFlatFunction(declaration, env, instance.context());
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        Environment environment = new Environment(closure);
        for (int i = 0; i < declaration.arguments.size(); i++) {
            environment.define(
                    declaration.arguments.get(i).lexeme,
                    arguments.get(i)
            );
        }

        try {
            interpreter.executeBlock(declaration.body, environment);
        } catch (Interpreter.Return returnValue) {
            return returnValue.value;
        }

        return new CFlatValue(false, CFlatNullClass.theInstance);
    }

    @Override
    public int argumentCount() {
        return declaration.arguments.size();
    }

    @Override
    public boolean mut() {
        return declaration.mut;
    }

    @Override
    public CFlatContext binding() {
        return binding;
    }

    @Override
    public String toString() {
        if (declaration.name == null) {
            return "<lambda>";
        }
        return "<function " + declaration.name.lexeme + ">";
    }
}
