package org.elsys.kristyanYochev.cflat.vm.builtins;

import org.elsys.kristyanYochev.cflat.vm.*;

import java.util.*;

public class CFlatNullClass extends CFlatClass {
    public static final CFlatNullClass theClass = new CFlatNullClass();
    public static final CFlatNull theInstance = new CFlatNull();

    private CFlatNullClass() {
        super("null", null, generateMethods(), new ArrayList<>());
    }

    @Override
    public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
        return new CFlatValue(false, theInstance);
    }

    @Override
    public int argumentCount() {
        return 0;
    }

    private static Map<String, Member<CFlatCallable>> generateMethods() {
        Map<String, Member<CFlatCallable>> methods = new HashMap<>();

        methods.put("__eq__", new Member<>(true, new CFlatNativeMethod<CFlatNull>(cFlatNull -> new CFlatNativeBoundMethod<>(cFlatNull) {
            @Override
            public CFlatValue call(Interpreter interpreter, List<CFlatValue> arguments) {
                assert boundInstance == theInstance;

                Object other = arguments.get(0).value();

                return new CFlatValue(false, CFlatBoolClass.javaBoolToCFlatBool(other == theInstance));
            }

            @Override
            public int argumentCount() {
                return 1;
            }
        })));

        return methods;
    }

    public static class CFlatNull extends CFlatClassInstance {
        private CFlatNull() {
            super(theClass);
        }
    }
}
