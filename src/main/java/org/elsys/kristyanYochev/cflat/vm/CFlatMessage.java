package org.elsys.kristyanYochev.cflat.vm;

import java.util.*;

public class CFlatMessage {
    public final String name;
    public final List<CFlatValue> arguments;

    public CFlatMessage(String name, List<CFlatValue> arguments) {
        this.name = name;
        this.arguments = arguments;
    }
}
