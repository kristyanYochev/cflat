package org.elsys.kristyanYochev.cflat;

import org.elsys.kristyanYochev.cflat.ast.Stmt;
import org.elsys.kristyanYochev.cflat.vm.Interpreter;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CFlat {
    public static final int STATIC_ANALYSIS_ERROR_CODE = 65;
    public static final int RUNTIME_ERROR_CODE = 70;

    public static void main(String[] args) throws IOException {
        if (args.length == 1) {
            runFile(args[0]);
        } else {
            System.err.println("Usage: cflat [filename]");
        }
    }

    private static void runFile(String filename) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(filename));
        ErrorHandler handler = new ErrorHandler();
        run(filename, new String(bytes, Charset.defaultCharset()), handler);

        if (handler.hadError()) System.exit(STATIC_ANALYSIS_ERROR_CODE);
        if (handler.hadRuntimeError()) System.exit(RUNTIME_ERROR_CODE);
    }

    private static void run(String filename, String source, ErrorHandler handler) {
        Lexer lexer = new Lexer(source, filename, handler);
        List<Token> tokens = lexer.scanTokens();

        if (handler.hadError()) return;

        Parser parser = new Parser(tokens, handler);
        List<Stmt> statements = parser.parse();

        if (handler.hadError()) return;

        Interpreter interpreter = new Interpreter(handler);

        Resolver resolver = new Resolver(interpreter, handler);
        resolver.resolve(statements);

        if (handler.hadError()) return;

        interpreter.interpret(statements);
    }
}
